#!/usr/bin/env python
# coding: utf8
import openpyxl
import pandas
import json
import requests
import argparse
import yaml
import datetime
import shutil

ALL_ENDPOINTS = {
    "dev": {
        "token": "https://explore.api.aai.ebi.ac.uk/auth",
        "validate": "https://wwwdev.ebi.ac.uk/biosamples/validate",
        "submit": "https://wwwdev.ebi.ac.uk/biosamples/samples/",
        "update": "https://wwwdev.ebi.ac.uk/biosamples/samples/"
    },
    "stable": {
        "token": "https://api.aai.ebi.ac.uk/auth",
        "validate": "https://www.ebi.ac.uk/biosamples/validate",
        "submit": "https://www.ebi.ac.uk/biosamples/samples/",
        "update": "https://www.ebi.ac.uk/biosamples/samples/"
    }
}


# Function to converts Excel file to JSON format
def excel_to_json(excel_file):
    # 'excel_file' = path to desired Excel file
    # Read Excel document and transform 'Material&Samples' worksheet into a dataframe with the 'Biological material' column as index
    excel_data_df = (pandas.read_excel(excel_file, sheet_name='Material&Samples')).set_index("Biological material ID*")
# Convert the dataframe to json format and load it in a dictionary called 'json_format_data'
    json_format_data = json.loads(excel_data_df.to_json(orient='index'))
    return json_format_data


# Genereates NCBI taxonomy link based on input taxon identifier
def generate_taxon_link(taxonID):
    if len(taxonID) < 9:
        return "wrong taxon identifier", taxonID
    else:
        ncbi_url = "http://purl.obolibrary.org/obo/NCBITaxon_"
        return ncbi_url + taxonID[10:]


# Creates a characteristic array used by ENA BioSamples
def characteristic(text, ontology=None):
    """
    Making a ENA Biosamples characteristic
    """
    # Check if 'ontology' parameter is provided
    if ontology:
        return [{"text": text, "ontologyTerms": [ontology]}]
    else:
        return [{"text": text}]


# Generates dictionary containing metadata for a given accession number
def generate_dict(json_format_data, accession):
    # add "doi:" before the doi to make it comply with the MIAPPE checklist
    if json_format_data[accession]['Material source DOI'] is not None:
        json_format_data[accession]['Material source DOI'] = "doi:"+json_format_data[accession]['Material source DOI']
        # Create a DOI URL
        doi = "https://doi.org/"+json_format_data[accession]['Material source DOI'][4:]
    else:
        doi = None

    if json_format_data[accession]['Plant structure development stage ontology'] is not None:
        psds = json_format_data[accession]['Plant structure development stage ontology']
    else:
        psds = None
    
    # Generate JSON object for sample metadata
    # if dev domain = "self.TestAgroDiv"
    # if not dev domain = self.AgroDivProject
    sample_json = {"name" : accession,
            "release": str(datetime.datetime.utcnow()).replace(" ","T"),
            "domain":"self.AgroDivProject",
            "characteristics" : {
                "project name" :
                    characteristic(json_format_data[accession]['Study unique ID*']),
                "biological material ID" :
                    characteristic(accession),
                "organism" :
                    characteristic(json_format_data[accession]['Organism_Name*'],ontology=generate_taxon_link(json_format_data[accession]['Organism_NCBI*'])),
                "genus" :
                    characteristic(json_format_data[accession]['Genus*']),
                "species" :
                    characteristic(json_format_data[accession]['Species*']),
                "infraspecific name" :
                    characteristic(json_format_data[accession]['Infraspecific name']),
                "ecotype" :
                    characteristic(json_format_data[accession]['Ecotype']),
                "biological material latitude" :
                    characteristic(json_format_data[accession]['Biological material latitude']),
                "biological material longitude" :
                    characteristic(json_format_data[accession]['Biological material longitude']),
                "biological material altitude" :
                    characteristic(json_format_data[accession]['Biological material altitude']),
                "biological material coordinates uncertainty" :
                    characteristic(json_format_data[accession]['Biological material coordinates uncertainty']),
                "biological material geographic location" :
                    characteristic(json_format_data[accession]['Biological material geographic location*']),
                "biological material preprocessing" :
                    characteristic(json_format_data[accession]['Biological material preprocessing']),
                "sex" :
                    characteristic(json_format_data[accession]['Sex*']),
                "environment (biome)" :
                    characteristic(json_format_data[accession]['Environment (biome)'], ontology=json_format_data[accession]['Environment (biome) ontology']),
                "environment parameter" :
                    characteristic(json_format_data[accession]['Environment parameter']),
                "environment parameter value" :
                    characteristic(json_format_data[accession]['Environment parameter value']),
                "plant structure development stage" :
                    characteristic(json_format_data[accession]['Plant structure development stage'], ontology=psds),
                "plant anatomical entity" :
                    characteristic(json_format_data[accession]['Plant anatomical entity*'], ontology=json_format_data[accession]['Plant anatomical entity ontology*']),
                "sample description" :
                    characteristic(json_format_data[accession]['Sample description']),
                "sample ID" :
                    characteristic(json_format_data[accession]['Sample ID*']),
                "material source ID" :
                    characteristic(json_format_data[accession]['Material source ID*']),
                #"material source instcode" :
                #    characteristic(json_format_data[accession]['Material source Instcode']),
                #"material source instname" :
                #    characteristic(json_format_data[accession]['Material source Instname']),
                #"material source accname" :
                #    characteristic(json_format_data[accession]['Material source Accname']),
                #"material source othernumb" :
                #    characteristic(json_format_data[accession]['Material source othernumb']),
                "material source DOI" :
                    characteristic(json_format_data[accession]['Material source DOI'], ontology=doi),
                "material source latitude" :
                    characteristic(json_format_data[accession]['Material source latitude']),
                "material source longitude" :
                    characteristic(json_format_data[accession]['Material source longitude']),
                "material source altitude" :
                    characteristic(json_format_data[accession]['Material source altitude']),
                "material source coordinates uncertainty" :
                    characteristic(json_format_data[accession]['Material source coordinates uncertainty']),
                "material source geographic location" :
                    characteristic(json_format_data[accession]['Material source geographic location*']),
                "biological material ploidy" :
                    characteristic(json_format_data[accession]['Biological material ploidy*']),
                "collection date" :
                    characteristic(json_format_data[accession]['Collection date*']),
                "checklist" :
                    characteristic("BSDC00002"),
            }}
    return sample_json


# Requests authentification token using specified credentials
def fetch_token(endpoint, username, password):
    #request a token using the AAP username and password
    token = requests.get(endpoint, auth=(username, password))
    if token.status_code != 200:
        print(f"Problem with request: {str(token)}")
        raise RuntimeError("Non-200 status code")
    return token.text


# Send POST request to submit data using the obtained token
def fetch_POST(endpoint, token, data):
    # request the submission of data using the token previously requested
    headers = {}
    headers['Accept'] = "application/hal+json"
    headers['Content-Type'] = "application/json;charset=UTF-8"
    headers['Authorization'] = f"Bearer {token}"
    r = requests.post(endpoint, data=data, headers=headers)
    if r.status_code == 201:
        return r.text
    elif r.status_code == 200:
        return r.text
    elif r.status_code != 200:
        print(f"Problem with request: {r.text}")
        raise RuntimeError("Non-200 status code")


# Sends PUT request to update existing data using the obtained token
def fetch_PUT(endpoint, token, data, accession):
    """
    Fetch single BrAPI object by path
    """
    url = endpoint+accession
    print('  PUT ' + url)
    headers = {}
    headers['Accept'] = "application/hal+json"
    headers['Content-Type'] = "application/json;charset=UTF-8"
    headers['Authorization'] = f"Bearer {token}"
    r = requests.put(url, data=data, headers=headers)
    if r.status_code == 201:
        return r.json()
    elif r.status_code != 200:
        print(f"Problem with request: {str(r)}")
        return r.text


# Modifies original Excel file to include new accession numbers
def add_data(list_to_insert, fichier, column, column_name):
    # Backup the original file
    shutil.copyfile(fichier+'.xlsx', fichier+'_backup.xlsx')
    # Load workbook and select 'Material&Samples' sheet
    book = openpyxl.load_workbook(fichier+'_backup.xlsx')
    sheet = book["Material&Samples"]
    # Define  columnns containing necessary information
    col = sheet[column]
    # Iterate over loaded column and list_to_insert simultaneously
    for row2, i in zip(col, range(len(list_to_insert))):
        if row2.value != column_name:
            row2.value = list_to_insert[i]
            book.save(fichier+'.xlsx')


def main(secret=".secret.yml"):
    """ Submit samples to BioSamples """
    parser = argparse.ArgumentParser()
    parser.add_argument("-dev", help="If development server submission",
                        action="store_true")
    parser.add_argument("-update", help="If it's an update",
                        action="store_true")
    parser.add_argument("-secret", help="Path to the secret file")
    parser.add_argument("-file", help="Path to metadata file")
    args = parser.parse_args()
    fichier = args.file

    # Load excel file data into data variable
    data = excel_to_json(fichier+'.xlsx')

    if args.dev:
        print("--- This is a test submission ---")
        ENDPOINTS = ALL_ENDPOINTS['dev']
    else:
        ENDPOINTS = ALL_ENDPOINTS['stable']

    print("--- Requesting TOKEN ---")
    secret_file = open(args.secret, "r")
    credentials = yaml.load(secret_file, Loader=yaml.FullLoader)

    password = credentials['password'].strip()
    username = credentials['username'].strip()
    token = fetch_token(ENDPOINTS['token'], username, password)
    print("  Authentication is successful")

    # Liste de Sample ID
    bsid = [0]
    # For each line (accession) of the datafile
    for accession in data:
        # Create a dict with the data
        sample_json = generate_dict(data, accession)
        # Liste qui va contenir les champs vides pour les supprimer du dictionnaire
        empty_characteristics = []
        # for each field of the accession
        for characteristic in data[accession]:
            # if the field is empty add the field to a list
            if data[accession][characteristic] is None:
                if characteristic == 'Environment (biome) ontology' or characteristic == 'Plant structure development stage ontology':
                    pass
                else:
                    empty_characteristics.append(characteristic)
        # for each empty field
        for empty_charac in empty_characteristics:
            if empty_charac == "Sample ID*":
                del sample_json["characteristics"][empty_charac[0].lower()+empty_charac[1:-1]]
            else :
                del sample_json["characteristics"][empty_charac[0].lower()+empty_charac[1:]]

        if args.update:
            sample_json["accession"] = sample_json["characteristics"]["sample ID"][0]['text']
            del sample_json["characteristics"]["sample ID"]
            print(
                f"  - Updating the JSON-LD schema of {sample_json['name']}")
            submit = fetch_PUT(
                ENDPOINTS['update'], token, json.dumps(sample_json), sample_json["accession"])
            print(submit)

        else:
            print(f"  - Validating the JSON-LD schema of {sample_json['name']}")
            fetch_POST(
                ENDPOINTS['validate'], token, json.dumps(sample_json))

            print(
                f"  - Submitting the JSON-LD schema of {sample_json['name']}")
            submit = json.loads(fetch_POST(
                        ENDPOINTS['submit'], token, json.dumps(sample_json)))

            bsid.append(submit["accession"])

    add_data(bsid, fichier, 'Y', 'Sample ID*')


if __name__ == "__main__":
    main()
