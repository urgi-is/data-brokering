 # Sample submission and update to BioSamples 

This project generates metadata in JSON-LD format for plant and animal biological samples and submits them to the European Nucleotide Archive (ENA)'s BioSamples database. The metadata is extracted from an Excel file and validated against the Plant MIAPPE checklist for plant samples and against the BioSamples minimal checklist for animal samples. Samples are then either submitted as new entries or updated if they already exist in the database.

## Dependencies

* Python 3.6 or higher
* OpenPyXL
* Pandas
* Requests
* Argparse
* Click
* PyYAML
* Datetime
* Shutil

## Usage

Before running the script, ensure that the following steps have been taken:

For the plant samples metadata template:
1. Remove the first three example rows in the 'Material&Samples' worksheet.
2. Remove the first column ('ENA JSON-MIAPPE').
3. Remove columns that are not yet included in the Plant MIAPPE checklist ("Material source Instcode", "Material source Instname", "Material source ACCNAME", "Material source OTHERNUMB").

For the animal samples metadata template:
1. Remove the first two example rows in the 'materials&samples' worksheet.
2. Remove the first column
3. Make sure that field names (first row) does not include spaces.

Once these steps have been completed, run the script using the following command:

For plant samples :
```
python3 submit_data_biosamples_plant.py [-dev] [-update] [-secret SECRET] [-file filename]
```
For animal samples :
```
python3 submit_data_biosamples_animal.py [-dev] [-update] [-secret SECRET] [-file filename]
```

The optional arguments are:

* `-dev`: use the development server for submission instead of the production server.
* `-update`: update existing samples instead of creating new ones.
* `-secret`: specify the path to the YAML file containing authentication details. If not provided, the default path is "./.secret.yml".

The script expects the Excel file to be located in the same directory as the script.  After processing, the script will modify the original Excel file to include generated BioSamples IDs.

Note: Before submitting the metadata to the BioSamples database, please register for an account and obtain an API key at <https://aai.ebi.ac.uk/login> for the production server and at <https://explore.aai.ebi.ac.uk/login> for the development server. Update the `.secret.yml` file accordingly.


