#!/usr/bin/env python
# coding: utf8
import sys

import openpyxl
import pandas
import json
import requests
import argparse
import yaml
import datetime
import shutil

ALL_ENDPOINTS = {
    "dev": {
        "token": "https://wwwdev.ebi.ac.uk/ena/submit/webin/auth/token",
        "validate": "https://wwwdev.ebi.ac.uk/biosamples/validate",
        "submit": "https://wwwdev.ebi.ac.uk/biosamples/samples/",
        "update": "https://wwwdev.ebi.ac.uk/biosamples/samples/"
    },
    "stable": {
        "token": "https://www.ebi.ac.uk/ena/submit/webin/auth/token",
        "validate": "https://www.ebi.ac.uk/biosamples/validate",
        "submit": "https://www.ebi.ac.uk/biosamples/samples/",
        "update": "https://www.ebi.ac.uk/biosamples/samples/"
    }
}

# Functions to converts Excel file to JSON format
##Converts Materials&samples sheet data into JSON format
def excel_to_json_ms(excel_file):
    # 'excel_file' = path to desired excel file
    # Read excel document and transform 'Material&Samples' worksheet into a dataframe with the 'Biological material' column as index
    excel_data_df = (pandas.read_excel(excel_file, sheet_name='materials&samples')).set_index("Biological material (=animal) ID*")
    # Convert the dataframe to json format and load it in a dictionary called 'json_format_data'
    json_format_data = json.loads(excel_data_df.to_json(orient='index'))
    return(json_format_data)

##Converts contact sheet data into JSON format
def excel_to_json_c(excel_file):
    # 'excel_file' = path to desired excel file
    # Read excel document and transform 'contact' worksheet into a dataframe with the 'Person name' column as index
    contact_df = (pandas.read_excel(excel_file, sheet_name='contact')).set_index("Person name")
    # Convert the dataframe to json format and load it in a dictionary called 'json_format_contact'
    json_format_contact = json.loads(contact_df.to_json(orient='index'))
    return(json_format_contact)

# Genereates NCBI taxonomy link based on input taxon identifier
def generate_taxon_link(taxonID):
    if not (taxonID.startswith("NCBITAXON:") or taxonID.startswith("http:")):
        raise ValueError("Field Organism_NCBI must be filled with the NCBI identifier (NCBITAXON:XXXX) or with the URL of the ontology of the taxon (http://purl.obolibrary.org/obo/NCBITaxon_XXXX)")
    else :
        if taxonID.startswith("http:") :
            return(taxonID)
        else :
            ncbi_url = "http://purl.obolibrary.org/obo/NCBITaxon_"
            return ncbi_url + taxonID[10:]

# Creates a characteristic array used by ENA BioSamples
def characteristic(text, ontology=None):
    """
    Making a ENA Biosamples characteristic
    """
    # Check if 'ontology' parameter is provided
    if ontology:
        return [{"text": text, "ontologyTerms": [ontology]}]
    else:
        return [{"text": text}]

def generate_doi_url(doi):
    if doi is not None :
        if not doi.startswith("http"):
            if not doi.startswith("doi"):
                doi_url = "https://doi.org/doi:"+doi
            else :
                doi_url = "https://doi.org/"+doi[4:]
        else :
            doi_url = doi
    else:
        doi_url = None
    return(doi_url)

def generate_doi(doi):
    if doi is not None :
        if doi.startswith("http"):
            doi = doi.split("https://doi.org/")[1]
        if doi.startswith("doi"):
            doi = doi.split(":")[-1]
    else:
        doi = None
    return(doi)

#Generates dictionary containing metadata for a given accession number
def generate_dict(json_format_data,accession):
    # Generate JSON object for sample metadata
    sample_json = {"name" : accession,
                   "release": str(datetime.datetime.utcnow()).replace(" ","T"),
                   "characteristics" : {
                       "Project name" :
                           characteristic(json_format_data[accession]['Study unique ID*']),
                       "Biological material ID" :
                           characteristic(accession),
                       "Organism" :
                           characteristic(json_format_data[accession]['Organism_Name'],ontology = generate_taxon_link(json_format_data[accession]['Organism_NCBI*'])),
                       "Genus" :
                           characteristic(json_format_data[accession]['Genus*']),
                       "Species" :
                           characteristic(json_format_data[accession]['Species*']),
                       "Infraspecific name" :
                           characteristic(json_format_data[accession]['Infraspecific name']),
                       "Breed ontology" :
                           characteristic(json_format_data[accession]['Breed ontology']),
                       "Type of line" :
                           characteristic(json_format_data[accession]['Type of line']),
                       "Ecotype / line within a breed" :
                           characteristic(json_format_data[accession]['Ecotype / line within a breed']),
                       "Comment on the line" :
                           characteristic(json_format_data[accession]['Comment on the line']),
                       "Sire ID" :
                           characteristic(json_format_data[accession]['Sire ID']),
                       "Dam ID" :
                           characteristic(json_format_data[accession]['Dam ID']),
                       "Phenotypic database" :
                           characteristic(json_format_data[accession]['Phenotypic database']),
                       "Biological material latitude" :
                           characteristic(json_format_data[accession]['Biological material latitude']),
                       "Biological material longitude" :
                           characteristic(json_format_data[accession]['Biological material longitude']),
                       "Biological material altitude" :
                           characteristic(json_format_data[accession]['Biological material altitude']),
                       "Biological material coordinates uncertainty" :
                           characteristic(json_format_data[accession]['Biological material coordinates uncertainty']),
                       "Biological material geographic location" :
                           characteristic(json_format_data[accession]['Biological material geographic location*']),
                       "Biological material preprocessing" :
                           characteristic(json_format_data[accession]['Biological material preprocessing']),
                       "Sex" :
                           characteristic(json_format_data[accession]['Sex*']),
                       "Type of production" :
                           characteristic(json_format_data[accession]['Type of production']),
                       "Reference list for type of productionontology" :
                           characteristic(json_format_data[accession]['Reference list for type of productionontology']),
                       "Farming system" :
                           characteristic(json_format_data[accession]['Farming system'], ontology=json_format_data[accession]["Farming system ontology"]),
                       "Animal development stage" :
                           characteristic(json_format_data[accession]['Animal development stage'], ontology= json_format_data[accession]['Animal development stage ontology']),
                       "Health status" :
                           characteristic(json_format_data[accession]['Health status'], ontology=json_format_data[accession]['Health status ontology']),
                       "Date of birth of the animal" :
                           characteristic(json_format_data[accession]['Date of birth of the animal']),
                       "Collection date" :
                           characteristic(json_format_data[accession]['Collection date of the sample*']),
                       "Sample ID" :
                           characteristic(json_format_data[accession]['Sample ID*']),
                       "Biosamples ID" :
                            characteristic(json_format_data[accession]["Biosamples ID"]),
                       "Anatomical entity" :
                           characteristic(json_format_data[accession]['Anatomical entity*'], ontology=json_format_data[accession]['Animal anatomical entity ontology*']),
                       "Tissue" :
                           characteristic(json_format_data[accession]['Tissue'], ontology=json_format_data[accession]['Tissue ontology']),
                       "Sample description" :
                            characteristic(json_format_data[accession]["Sample description"]),
                       "Sample processing protocol" :
                            characteristic(json_format_data[accession]["Sample processing protocol"]),
                       "Pre-existing molecular data on this sample" :
                            characteristic(json_format_data[accession]["Pre-existing molecular data on this sample"]),
                       "Analytical protocol in AgroDiv" :
                            characteristic(json_format_data[accession]["Analytical protocol in AgroDiv"]),
                       "Material source ID" :
                           characteristic(json_format_data[accession]['Material source ID*']),
                       "Material source accession name" :
                           characteristic(json_format_data[accession]['Material source ACCNAME']),
                       "Material source DOI" :
                           characteristic(generate_doi(json_format_data[accession]['Material source DOI']), ontology = generate_doi_url(json_format_data[accession]['Material source DOI'])),
                       "Geographic location (country and/or sea)" :
                           characteristic(json_format_data[accession]['Material source geographic location']),
                       "checklist" :
                           characteristic("BSDC00001"),
                   },
                   "contact" : [{
                       "E-mail" : "no@email.com",
                       "Affiliation" : "affiliation",
                       "Name" : "no one"
                   }]}
    return(sample_json)


# Requests authentification token using specified credentials

# #request a token using the AAP username and password
# def fetch_token(endpoint, username, password):
#     #request a token using the AAP username and password
#     token = requests.get(endpoint, auth=(username, password))
#     if token.status_code != 200:
#         print(f"Problem with request: {str(token)}")
#         raise RuntimeError("Non-200 status code")
#     return token.text


#request a token using the Webin username and password
def fetch_token(endpoint, username, password):
    auth_body = {
        "authRealms": ["ENA"],
        "password": password,
        "username": username
    }
    response = requests.post(endpoint, json=auth_body)
    if response.status_code == requests.codes.ok:
        return response.text
    else:
        raise response.raise_for_status()

# Send POST request to submit data using the obtained token
def fetch_POST(endpoint, token, data):
    #request the submission of data using the token previously requested
    headers = {}
    headers['Accept'] = "application/hal+json"
    headers['Content-Type'] = "application/json;charset=UTF-8"
    headers['Authorization'] = f"Bearer {token}"
    r = requests.post(endpoint, data=data, headers=headers)
    if r.status_code == 201:
        return r.text
    elif r.status_code == 200 :
        return r.text
    elif r.status_code != 200:
        print(f"Problem with request: {r.text}")
        raise RuntimeError("Non-200 status code")


# Sends PUT request to update existing data using the obtained token
def fetch_PUT(endpoint, token, data, accession):
    """
    Fetch single BrAPI object by path
    """
    url = endpoint+accession
    print('  PUT ' + url)
    headers = {}
    headers['Accept'] = "application/hal+json"
    headers['Content-Type'] = "application/json;charset=UTF-8"
    headers['Authorization'] = f"Bearer {token}"
    r = requests.put(url, data=data, headers=headers)
    if r.status_code == 201:
        return r.json()
    elif r.status_code != 200:
        print(f"Problem with request: {str(r)}")
        return r.status_code


#Modifies original Excel file to include new accession numbers
def add_data(list_to_insert,fichier,column,column_name):
    # Backup the original file
    shutil.copyfile(fichier+'.xlsx', fichier+'_backup.xlsx')
    #Load workbook and select 'Material&Samples' sheet
    book = openpyxl.load_workbook(fichier+'_backup.xlsx')
    sheet = book["materials&samples"]
    #Define  columnns containing necessary informations
    col = sheet[column]
    #Iterate over loaded column and list_to_insert simultaneously
    for row2, i in zip(col,range(len(list_to_insert))):
        if row2.value != column_name:
            row2.value = list_to_insert[i]
            book.save(fichier+'.xlsx')



def main():
    """ Submit samples to BioSamples """
    parser = argparse.ArgumentParser()
    parser.add_argument("-dev", help="If development server submission",
                        action="store_true")
    parser.add_argument("-update", help="If it's an update",
                        action="store_true")
    parser.add_argument("-account", help="Path to the file containing your login details")
    parser.add_argument("-file", help="Path to metadata file")
    args = parser.parse_args()
    fichier = args.file

    #Load excel file data into data variable
    data = excel_to_json_ms(fichier + '.xlsx')

    if args.dev:
        print("--- This is a test submission ---")
        ENDPOINTS = ALL_ENDPOINTS['dev']
    else:
        ENDPOINTS = ALL_ENDPOINTS['stable']

    print("--- Requesting TOKEN ---")
    secret_file = open(args.account, "r")
    credentials = yaml.load(secret_file, Loader=yaml.FullLoader)

    password = credentials['password'].strip()
    username = credentials['username'].strip()
    token = fetch_token(ENDPOINTS['token'], username, password)
    print("  Authentication is successful")

    #Sample ID list
    bsid = [0]
    #For each line (accession) of the datafile
    for accession in data :
        #Create a dict with the data
        sample_json = generate_dict(data,accession)
        #Create a list for empty fields to delete them from the dict
        empty_characteristics = []
        #for each field of the accession
        for characteristic in data[accession] :
            #if the field is empty add the field to the list (except if it is an ontology field)
            if data[accession][characteristic] is None :
                if (characteristic =='Environment (biome) ontology' or
                    characteristic =='Animal development stage ontology' or
                    characteristic =='Farming system ontology' or
                    characteristic =='Health status ontology' or
                    characteristic == "Tissue ontology"):
                    pass
                else:
                    empty_characteristics.append(characteristic)

        #for each empty field delete the field from the dict
        for empty_charac in empty_characteristics:
            if empty_charac == 'Material source ACCNAME' :
                del sample_json["characteristics"]["Material source accession name"]
            elif empty_charac == "Material source geographic location":
                del sample_json["characteristics"]["Geographic location (country and/or sea)"]
            else:
                del sample_json["characteristics"][empty_charac]

        #Create a dict and a list for contacts
        contact_json = {}
        contact_list = []
        #for each contact line create a contact object and add it to the list
        for contact in excel_to_json_c(fichier+ '.xlsx'):
            contact_list.append({"E-mail" : excel_to_json_c(fichier+ '.xlsx')[contact]['Person e-mail '],"Affiliation" : excel_to_json_c(fichier+ '.xlsx')[contact]['affiliation_personne'],"Name" : contact})
        #load the contact list into the contact dict
        contact_json["contact"] = contact_list

        #if it is an update submission load the biosample ID into the accession field and delete it from the biosamples field
        #concatenate sample json and contact json and submit it
        if args.update:
            sample_json["accession"] = sample_json["characteristics"]["Biosamples ID"][0]['text']
            del sample_json["characteristics"]["Biosamples ID"]
            all_json = {**sample_json,**contact_json}
            print(
                f"  - Updating the JSON-LD schema of {sample_json['name']}")
            submit = fetch_PUT(
                ENDPOINTS['update'], token, json.dumps(all_json), sample_json["accession"])
            if submit is not None:
                print(submit)

        #if this is not an update concatenate the sample and contact dict and validate it and submit it
        else :
            all_json = {**sample_json,**contact_json}
            print(f"  - Validating the JSON-LD schema of {sample_json['name']}")
            validate = fetch_POST(
               ENDPOINTS['validate'], token, json.dumps(all_json))

            print(
                f"  - Submitting the JSON-LD schema of {sample_json['name']}")
            submit = json.loads(fetch_POST(
               ENDPOINTS['submit'], token, json.dumps(all_json)))

            #add the biosample id to the list of biosamples ids
            bsid.append(submit["accession"])
    #add the biosample id to the file
    add_data(bsid,fichier,'AG','Biosamples ID')




if __name__ == "__main__":
    main()